CREATE DATABASE `foo`;
USE `foo`;

CREATE TABLE `source` (
	`a` INT UNSIGNED NOT NULL,
	`b` INT UNSIGNED NOT NULL,
	`c` INT UNSIGNED NOT NULL,
	PRIMARY KEY (`a`)
) ENGINE=InnoDB;
