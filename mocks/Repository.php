<?php

namespace Mocks;

use App\Model\Source;
use App\Repository\RepositoryInterface;

class Repository implements RepositoryInterface
{
    public bool $seedingOK = true;

    public int $seeded = 0;

    public int $page = 0;

    public int $perPage = 0;

    public array $pageData = [];

    public function count(): int
    {
        return 0;
    }

    public function addBatch(array $sources)
    {
        /** @var Source $source */
        foreach ($sources as $source) {
            if ($source->a % 3 !== $source->b || $source->a % 5 !== $source->c) {
                $this->seedingOK = false;
                break;
            }
            $this->seeded++;
        }
    }

    public function page(int $page, int $perPage): array
    {
        $this->page = $page;
        $this->perPage = $perPage;

        return $this->pageData;
    }
}
