<?php

namespace Mocks;

use App\Controller\ControllerInterface;

class Controller implements ControllerInterface
{
    private \Closure $reader;

    private ?\Exception $pageException;

    private array $pageData;

    public function __construct(\Closure $reader, ?\Exception $pageException, array $pageData)
    {
        $this->reader = $reader;
        $this->pageException = $pageException;
        $this->pageData = $pageData;
    }

    public function seed(int $count)
    {
    }

    public function reader(): \Closure
    {
        return $this->reader;
    }

    public function page(int $page, int $perPage): array
    {
        if ($this->pageException) {
            throw $this->pageException;
        }

        return $this->pageData;
    }
}
