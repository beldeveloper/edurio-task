<?php

namespace Mocks;

use Core\Http\ResponseInterface;

class Response implements ResponseInterface
{
    public bool $chunkStarted;

    public bool $chunkEnded;

    public int $status = 200;

    public int $chunksCount = 0;

    public $responseJson;

    public function startChunk()
    {
        $this->chunkStarted = true;
    }

    public function sendChunk(string $chunk)
    {
        $this->chunksCount++;
    }

    public function endChunk()
    {
        $this->chunkEnded = true;
    }

    public function respondJson($data)
    {
        $this->responseJson = $data;
    }

    public function respondText(string $text)
    {
    }

    public function setStatus(int $status)
    {
        $this->status = $status;
    }
}
