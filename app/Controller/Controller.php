<?php

namespace App\Controller;

use App\Model\Source;
use App\Repository\RepositoryInterface;

class Controller implements ControllerInterface
{
    const SEED_BATCH_SIZE = 1000;

    const READ_BATCH_SIZE = 1000;

    const MAX_PER_PAGE = 50000;

    const DEFAULT_PER_PAGE = 1000;

    private RepositoryInterface $repository;

    public function __construct(RepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    public function seed(int $count)
    {
        $batch = [];

        for ($i = 1; $i <= $count; $i++) {
            $source = new Source();
            $source->a = $i;
            $source->b = $i % 3;
            $source->c = $i % 5;
            $batch[] = $source;

            if (count($batch) === self::SEED_BATCH_SIZE || $i === $count) {
                $this->repository->addBatch($batch);
                $batch = [];
            }
        }
    }

    public function reader(): \Closure
    {
        $pages = ceil($this->repository->count() / self::READ_BATCH_SIZE);

        return function () use ($pages) {
            for ($page = 1; $page <= $pages; $page++) {
                $batch = $this->repository->page($page, self::READ_BATCH_SIZE);
                foreach ($batch as $source) {
                    yield $source;
                }
            }
        };
    }

    public function page(int $page, int $perPage): array
    {
        if ($page < 1) {
            $page = 1;
        }

        if ($perPage < 1) {
            $perPage = self::DEFAULT_PER_PAGE;
        } elseif ($perPage > self::MAX_PER_PAGE) {
            $perPage = self::MAX_PER_PAGE;
        }

        $data = $this->repository->page($page, $perPage);
        if (count($data) === 0) {
            throw new \Exception("page is invalid", 404);
        }

        return $data;
    }
}
