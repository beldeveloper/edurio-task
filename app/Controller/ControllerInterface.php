<?php

namespace App\Controller;

interface ControllerInterface
{
    public function seed(int $count);

    public function reader(): \Closure;

    public function page(int $page, int $perPage): array;
}
