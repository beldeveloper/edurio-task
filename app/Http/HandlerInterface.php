<?php

namespace App\Http;

interface HandlerInterface
{
    public function csv();

    public function page();
}
