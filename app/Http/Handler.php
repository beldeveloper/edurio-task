<?php

namespace App\Http;

use App\Controller\ControllerInterface;
use App\Model\Source;
use Core\Http\ResponseInterface;
use Core\Http\RouterInterface;

class Handler implements HandlerInterface
{
    private ControllerInterface $controller;

    private RouterInterface $router;

    private ResponseInterface $response;

    public function __construct(ControllerInterface $controller, RouterInterface $router, ResponseInterface $response)
    {
        $this->controller = $controller;
        $this->router = $router;
        $this->response = $response;
    }

    public function csv()
    {
        $reader = $this->controller->reader();
        $this->response->startChunk();

        try {
            /** @var Source $item */
            foreach ($reader() as $item) {
                $row = implode(",", [$item->a, $item->b, $item->c]) . "\n";
                $this->response->sendChunk($row);
            }
        } catch (\PDOException $e) {
            if ($e->getCode() === "42S02") {
                $this->response->setStatus(404);
                $this->response->sendChunk("Table is not found\n");
            } else {
                $this->response->setStatus(500);
                $this->response->sendChunk("Internal error\n");
            }
        } catch (\Exception $e) {
            if ($e->getCode() == 404) {
                $this->response->setStatus(404);
                $this->response->sendChunk($e->getMessage() . "\n");
            } else {
                throw $e;
            }
        }

        $this->response->endChunk();
    }

    public function page()
    {
        try {
            $data = $this->controller->page((int)$this->router->param('page'), (int)$this->router->param('page_size'));
        } catch (\PDOException $e) {
            if ($e->getCode() === "42S02") {
                $this->response->setStatus(404);
                $this->response->respondText("Table is not found");
            } else {
                $this->response->setStatus(500);
                $this->response->respondText("Internal error");
            }

            return;
        } catch (\Exception $e) {
            if ($e->getCode() == 404) {
                $this->response->setStatus(404);
                $this->response->respondText($e->getMessage());

                return;
            } else {
                throw $e;
            }
        }

        $this->response->respondJson($data);
    }
}
