<?php

namespace App\Console;

interface HandlerInterface
{
    public function seed(int $count);

    public function test();
}
