<?php

namespace App\Console;

use App\Controller\ControllerInterface;
use Core\Tests;

class Handler implements HandlerInterface
{
    private ControllerInterface $controller;

    public function __construct(ControllerInterface $controller)
    {
        $this->controller = $controller;
    }

    public function seed(int $count)
    {
        $this->controller->seed($count);
        echo sprintf("%d sources are generated\n", $count);
    }

    public function test()
    {
        (new Tests())->run();
    }
}
