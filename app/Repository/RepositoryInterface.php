<?php

namespace App\Repository;

interface RepositoryInterface
{
    public function count(): int;

    public function addBatch(array $sources);

    public function page(int $page, int $perPage): array;
}
