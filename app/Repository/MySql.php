<?php

namespace App\Repository;

use App\Model\Source;

class MySql implements RepositoryInterface
{
    private \PDO $conn;

    public function __construct(\PDO $conn)
    {
        $this->conn = $conn;
    }

    public function count(): int
    {
        return $this->conn->query("SELECT COUNT(*) FROM `source`")->fetchColumn();
    }

    public function addBatch(array $sources)
    {
        $sql = "INSERT INTO `source` VALUES";

        /** @var Source $source */
        foreach ($sources as $i => $source) {
            $sql .= sprintf(" (%d, %d, %d)", $source->a, $source->b, $source->c);
            if ($i < count($sources) - 1) {
                $sql .= ", ";
            }
        }

        $this->conn->prepare($sql)->execute();
    }

    public function page(int $page, int $perPage): array
    {
        // OFFSET works slow, in this case it is better to use condition for an indexed column
        // $sql = sprintf("SELECT `a`, `b`, `c` FROM `source` LIMIT %d OFFSET %d", $perPage, $perPage * ($page - 1));
        $sql = sprintf(
            "SELECT `a`, `b`, `c` FROM `source` WHERE `a` > %d ORDER BY `a` ASC LIMIT %d",
            $perPage * ($page - 1),
            $perPage
        );
        $stmt = $this->conn->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll(\PDO::FETCH_ASSOC);
        $res = [];

        foreach ($data as $row) {
            $source = new Source();
            $source->a = $row['a'];
            $source->b = $row['b'];
            $source->c = $row['c'];
            $res[] = $source;
        }

        return $res;
    }
}
