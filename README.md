# Edurio Test Task

## Run the project
`docker-compose up -d`

## Seed DB records
`docker-compose exec app php index.php seed 1000000`

## Run tests
`docker-compose exec app php index.php test`

## Notes

- URL - http://localhost:8082
- written from scratch just for fun (no frameworks/libs)
- in the real project, it is better to use framework/libraries for application (http, db, etc.) and for testing
- a lot of things could be improved (e.g. error handling, db migrations, etc.)
- DB is not available right after the container started due to init db sql
- tests cover only requirements defined in the task, so the total coverage is low
