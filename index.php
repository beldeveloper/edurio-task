<?php

use App\Controller\Controller;
use App\Repository\MySql;
use Core\Application\ConsoleApplication;
use Core\Application\HttpApplication;
use Core\Http\Response;
use Core\Http\Router;

const ROOT_DIR = __DIR__;

spl_autoload_register(function ($class) {
    $path = str_replace('\\', '/', $class);
    $path = lcfirst($path);
    require_once __DIR__ . "/{$path}.php";
});

$pdo = new PDO(
    sprintf("mysql:dbname=%s;host=%s", getenv("DB_NAME"), getenv("DB_HOST")),
    getenv("DB_USER"),
    getenv("DB_PASSWORD"),
    [
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
    ]
);
$repo = new MySql($pdo);
$ctrl = new Controller($repo);

try {
    if (PHP_SAPI === "cli") {
        $handler = new \App\Console\Handler($ctrl);
        $app = new ConsoleApplication($handler, $argv);
    } else {
        $router = new Router();
        $handler = new \App\Http\Handler($ctrl, $router, new Response());
        $app = new HttpApplication($handler, $router, [
            [
                "method" => "GET",
                "route"  => "/dbs/foo/tables/source/csv",
                "action" => "csv",
            ],
            [
                "method" => "GET",
                "route"  => "/dbs/foo/tables/source/json",
                "action" => "page",
            ],
        ]);
    }

    $app->run();
} catch (Exception $e) {
    echo "Error occurred: {$e->getMessage()}\n";
}
