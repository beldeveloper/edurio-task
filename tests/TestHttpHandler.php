<?php

namespace Tests;

use App\Http\Handler;
use App\Model\Source;
use Core\Http\Router;
use Mocks\Controller;
use Mocks\Response;

class TestHttpHandler
{
    public function testCsv()
    {
        $tt = [
            [
                "name"   => "internal error",
                "status" => 500,
                "count"  => 1,
                "reader" => function () {
                    throw new \PDOException("db error", 1111);
                    yield null;
                }
            ],
            [
                "name"   => "not found error",
                "status" => 404,
                "count"  => 1,
                "reader" => function () {
                    throw new \Exception("not found", 404);
                    yield null;
                }
            ],
            [
                "name"   => "success",
                "status" => 200,
                "count"  => 3,
                "reader" => function () {
                    for ($i = 1; $i <= 3; $i++) {
                        $source = new Source();
                        $source->a = $i;
                        $source->b = $i;
                        $source->c = $i;
                        yield $source;
                    }
                }
            ],
        ];

        foreach ($tt as $tc) {
            $ctrl = new Controller($tc["reader"], null, []);
            $response = new Response();
            $handler = new Handler($ctrl, new Router(), $response);
            try {
                $handler->csv();
            } catch (\Exception $e) {
                throw new \Exception($tc["name"] . ": Unexpected exception {$e->getMessage()}");
            }
            if ($response->status !== $tc["status"]) {
                throw new \Exception(
                    sprintf("%s: Expected HTTP status %d; got %d", $tc["name"], $tc["status"], $response->status)
                );
            }
            if (!$response->chunkStarted || !$response->chunkEnded) {
                throw new \Exception($tc["name"] . ": Invalid work with chunks");
            }
            if ($response->chunksCount !== $tc["count"]) {
                throw new \Exception(
                    sprintf("%s: Expected %d chunks; got %d", $tc["name"], $tc["count"], $response->chunksCount)
                );
            }
        }
    }

    public function testPage()
    {
        $tt = [
            [
                "name"      => "internal error",
                "status"    => 500,
                "exception" => new \PDOException("internal error", 1111),
            ],
            [
                "name"      => "not found error",
                "status"    => 404,
                "exception" => new \Exception("not found", 404),
            ],
            [
                "name"   => "success",
                "status" => 200,
                "data"   => [1, 2, 3],
            ],
        ];

        foreach ($tt as $tc) {
            $ctrl = new Controller(function () {}, $tc["exception"] ?: null, $tc["data"] ?: []);
            $response = new Response();
            $handler = new Handler($ctrl, new Router(), $response);
            try {
                $handler->page();
            } catch (\Exception $e) {
                throw new \Exception($tc["name"] . ": Unexpected exception {$e->getMessage()}");
            }
            if ($response->status !== $tc["status"]) {
                throw new \Exception(
                    sprintf("%s: Expected HTTP status %d; got %d", $tc["name"], $tc["status"], $response->status)
                );
            }
            if (!empty($tc["data"]) && $tc["data"] !== $response->responseJson) {
                throw new \Exception($tc["name"] . ": Invalid data responded");
            }
        }
    }
}
