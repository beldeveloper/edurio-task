<?php

namespace Tests;

use App\Controller\Controller;
use Mocks\Repository;

class TestController
{
    public function testSeed()
    {
        $tt = [
            [
                "name"  => "one batch",
                "count" => 10,
            ],
            [
                "name"  => "many batches",
                "count" => 1000000,
            ],
        ];

        foreach ($tt as $tc) {
            $repo = new Repository();
            $ctrl = new Controller($repo);
            try {
                $ctrl->seed($tc["count"]);
            } catch (\Exception $e) {
                throw new \Exception($tc["name"] . ": Unexpected exception {$e->getMessage()}");
            }
            if (!$repo->seedingOK) {
                throw new \Exception($tc["name"] . ": Invalid data");
            }
            if ($repo->seeded !== $tc["count"]) {
                throw new \Exception($tc["name"] . ": Invalid count");
            }
        }
    }

    public function testPage()
    {
        $tt = [
            [
                "name"         => "default values",
                "page"         => 0,
                "per_page"     => 0,
                "res_page"     => 1,
                "res_per_page" => Controller::DEFAULT_PER_PAGE,
                "data"         => [1, 2, 3],
            ],
            [
                "name"         => "too many per page",
                "page"         => 1,
                "per_page"     => Controller::MAX_PER_PAGE + 1,
                "res_page"     => 1,
                "res_per_page" => Controller::MAX_PER_PAGE,
                "data"         => [1, 2, 3],
            ],
            [
                "name"         => "not found",
                "page"         => 1,
                "per_page"     => 1,
                "res_page"     => 1,
                "res_per_page" => 0,
                "data"         => [],
            ],
        ];

        foreach ($tt as $tc) {
            $repo = new Repository();
            $repo->pageData = $tc["data"];
            $ctrl = new Controller($repo);
            try {
                $data = $ctrl->page($tc["page"], $tc["per_page"]);
            } catch (\Exception $e) {
                if (!($e->getCode() == 404 && empty($tc["data"]))) {
                    echo $e->getCode();
                    throw new \Exception($tc["name"] . ": Unexpected exception {$e->getMessage()}");
                }
                continue;
            }
            if ($repo->page !== $tc["res_page"]) {
                throw new \Exception(
                    sprintf("%s: Expected page %d; got %d", $tc["name"], $tc["res_page"], $repo->page)
                );
            }
            if ($repo->perPage !== $tc["res_per_page"]) {
                throw new \Exception(
                    sprintf("%s: Expected per page %d; got %d", $tc["name"], $tc["res_per_page"], $repo->perPage)
                );
            }
            if ($repo->pageData !== $data) {
                throw new \Exception($tc["name"] . ": Invalid page data");
            }
        }
    }
}
