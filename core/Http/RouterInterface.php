<?php

namespace Core\Http;

interface RouterInterface
{
    public function method(): string;

    public function route(): string;

    public function param(string $name);
}
