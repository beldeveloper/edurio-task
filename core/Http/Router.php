<?php

namespace Core\Http;

class Router implements RouterInterface
{
    public function method(): string
    {
        return $_SERVER['REQUEST_METHOD'];
    }

    public function route(): string
    {
        return explode('?', $_SERVER['REQUEST_URI'], 2)[0];
    }

    public function param(string $name)
    {
        return $_GET[$name] ?: null;
    }
}
