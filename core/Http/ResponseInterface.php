<?php

namespace Core\Http;

interface ResponseInterface
{
    public function startChunk();

    public function sendChunk(string $chunk);

    public function endChunk();

    public function respondJson($data);

    public function respondText(string $text);

    public function setStatus(int $status);
}
