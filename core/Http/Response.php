<?php

namespace Core\Http;

class Response implements ResponseInterface
{
    public function startChunk()
    {
        header("Transfer-Encoding: chunked");
        apache_setenv("no-gzip", 1);
    }

    public function sendChunk(string $chunk)
    {
        echo sprintf("%x\r\n", strlen($chunk));
        echo $chunk . "\r\n";
        flush();
    }

    public function endChunk()
    {
        echo "0\r\n\r\n";
        flush();
    }

    public function respondJson($data)
    {
        header("Content-Type: application/json");
        echo json_encode($data);
    }

    public function respondText(string $text)
    {
        echo $text;
    }

    public function setStatus(int $status)
    {
        http_response_code($status);
    }
}
