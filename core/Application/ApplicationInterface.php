<?php

namespace Core\Application;

interface ApplicationInterface
{
    public function run();
}
