<?php

namespace Core\Application;

use App\Http\HandlerInterface;
use Core\Http\RouterInterface;

class HttpApplication implements ApplicationInterface
{
    private HandlerInterface $handler;

    private RouterInterface $router;

    private array $routes;

    public function __construct(HandlerInterface $handler, RouterInterface $router, array $routes)
    {
        $this->handler = $handler;
        $this->router  = $router;
        $this->routes  = $routes;
    }

    public function run()
    {
        $method = strtolower($this->router->method());
        $path   = $this->router->route();

        foreach ($this->routes as $route) {
            if (strtolower($route['method']) === $method && $route['route'] === $path) {
                $this->handler->{$route['action']}();

                return;
            }
        }

        http_response_code(404);
        throw new \Exception('Route is not found');
    }
}
