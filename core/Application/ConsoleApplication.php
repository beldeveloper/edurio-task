<?php

namespace Core\Application;

use App\Console\HandlerInterface;

class ConsoleApplication implements ApplicationInterface
{
    private HandlerInterface $handler;

    private array $args;

    public function __construct(HandlerInterface $handler, array $args)
    {
        if (count($args) < 2) {
            throw new \Exception('Please specify command', 1001);
        }

        $this->handler = $handler;
        $this->args    = $args;
    }

    public function run()
    {
        $params = array_slice($this->args, 2);
        if (!method_exists($this->handler, $this->cmd())) {
            throw new \Exception('Invalid method', 1002);
        }

        $this->handler->{$this->cmd()}(...$params);
    }

    private function cmd(): string
    {
        return strtolower($this->args[1]);
    }
}
