<?php

namespace Core;

class Tests
{
    public function run()
    {
        $classes = $this->collectTests();
        foreach ($classes as $class) {
            $r = new \ReflectionClass($class);
            foreach ($r->getMethods() as $method) {
                $method = $method->getName();
                if (strpos($method, 'test') === 0) {
                    try {
                        $test = new $class;
                        $test->{$method}();
                    } catch (\Exception $e) {
                        echo "{$class}::{$method}() -> {$e->getMessage()}\n";
                    }
                }
            }
        }
    }

    private function collectTests(): array
    {
        $files = glob(ROOT_DIR . "/tests/Test*.php");

        return array_map(
            function ($file) {
                return "Tests\\" . preg_replace("/^.*\/(Test[^\/]+)\.php/", "$1", $file);
            },
            $files
        );
    }
}
